<?php

namespace Drupal\jira_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm
 * @package Drupal\jira_connector\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jira_connector_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'jira_connector.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('jira_connector.settings');

    $form['#tree'] = TRUE;

    $form['configuration'] = [
      '#type' => 'item',
      '#id' => 'configuration',
    ];

    $form['configuration']['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Jira host'),
      '#description' => $this->t('The https://your-jira.host.com url'),
      '#required' => TRUE,
      '#default_value' => $config->get('host'),
    ];

    $form['configuration']['credential_provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Credential provider'),
      '#options' => [
        'jira_connector' => 'Jira connector',
      ],
      '#default_value' => $config->get('credential_provider'),
      '#ajax' => [
        'callback' => [$this, 'ajaxCallback'],
        'wrapper' => 'configuration',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    $credential_provider = $form_state->getValue(['configuration', 'credential_provider'], $config->get('credential_provider'));

    if (\Drupal::moduleHandler()->moduleExists('key')) {
      $form['configuration']['credential_provider']['#options']['key'] = 'Key Module';

      /** @var \Drupal\key\Plugin\KeyPluginManager $key_type */
      $key_type = \Drupal::service('plugin.manager.key.key_type');
      if ($key_type->hasDefinition('user_password')) {
        $form['configuration']['credential_provider']['#options']['multikey'] = 'Key Module (user/password)';
      }
    }

    if ($credential_provider === 'jira_connector') {
      $form['configuration']['credentials']['jira_connector']['username'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Username'),
        '#description' => $this->t('A username required by the Jira host'),
        '#default_value' => $config->get('credentials.jira_connector.username'),
        '#attributes' => [
          'autocomplete' => 'off',
        ],
      ];
      $current_password = $config->get('credentials.jira_connector.password');
      $form['configuration']['credentials']['jira_connector']['password'] = [
        '#type' => 'password',
        '#title' => $this->t('Password'),
        '#description' => $this->t('A password required by the Jira host (leave blank if not required)'),
        '#default_value' => $current_password,
        '#attributes' => [
          'autocomplete' => 'off',
        ],
      ];

      if (!empty($current_password)) {
        $form['configuration']['credentials']['jira_connector']['password']['#description'] = $this->t('A password required by the Jira host. <em>The currently set password is hidden for security reasons</em>.');
      }
    }
    elseif ($credential_provider === 'key') {
      $form['configuration']['credentials']['key']['username'] = [
        '#type' => 'key_select',
        '#title' => $this->t('Username'),
        '#description' => $this->t('A username required by the Jira host.'),
        '#default_value' => $config->get('credentials.key.username'),
        '#empty_option' => $this->t('- Please select -'),
        '#key_filters' => ['type' => 'authentication'],
        '#required' => TRUE,
      ];
      $form['configuration']['credentials']['key']['password'] = [
        '#type' => 'key_select',
        '#title' => $this->t('Password'),
        '#description' => $this->t('A password required by the Jira host.'),
        '#default_value' => $config->get('credentials.key.password'),
        '#empty_option' => $this->t('- Please select -'),
        '#key_filters' => ['type' => 'authentication'],
        '#required' => TRUE,
      ];
    }
    elseif ($credential_provider === 'multikey') {
      $form['configuration']['credentials']['multikey']['user_password'] = [
        '#type' => 'key_select',
        '#title' => $this->t('User/password'),
        '#description' => $this->t('A username + password required by the Jira host.'),
        '#default_value' => $config->get('credentials.multikey.user_password'),
        '#empty_option' => $this->t('- Please select -'),
        '#key_filters' => ['type' => 'user_password'],
        '#required' => TRUE,
      ];
    }

    return $form;
  }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $config = $this->config('jira_connector.settings');
        $config->set('host', $form_state->getValue(['configuration', 'host']));
        $config->set('credential_provider', $form_state->getValue(['configuration', 'credential_provider']));
        $config->set('credentials', [
            $config->get('credential_provider') => $form_state->getValue(['configuration', 'credentials', $config->get('credential_provider')])
        ]);
        $config->save();
    }

  /**
   * Ajax callback for the dependent configuration options.
   *
   * @return array
   *   The form element containing the configuration options.
   */
  public static function ajaxCallback($form, FormStateInterface $form_state) {
    return $form['configuration'];
  }

}
